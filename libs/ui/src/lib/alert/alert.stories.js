
import Alert  from './alert';

export default {
  component: Alert,
  title: 'Alert', 
  argTypes: {
    messageColor:{
      control: 'color',
    },
    subTextColor:{
      control: 'color',
    },
    colorImg:{
      control: 'color',
    },
    backgroundCustom:{
      control:'color'
    },
    type:{
      options: ['warning',"success","info","error"],
      control: { type: 'select' }
    },
  }
};

const Template = (args) => <Alert {...args} />;

export const Primary = Template.bind({})

Primary.args = {
  message:"Hello there i am alert",
  subText:"more info"
}