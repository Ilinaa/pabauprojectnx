import styled from 'styled-components';
import { Alert as AlertItem } from 'antd';

const StyledAlert = styled(AlertItem)`
  background:${props => props.backgroundCustom};
  color:${props => props.color};
  .ant-alert-message{
    color:${props => props.messageColor};
  }
  .ant-alert-action{
    color:${props => props.subTextColor};
  }
  svg{
    color:${props => props.colorImg};
  }
`;
export function Alert({
  message,
  handleClick,
  subText,
  type,
  messageColor,
  subTextColor,
  colorImg,
  backgroundCustom,
}) {
  return (
    <StyledAlert
      banner
      message={message}
      messageColor={messageColor}
      type={type}
      subTextColor={subTextColor}
      colorImg={colorImg}
      backgroundCustom={backgroundCustom}
      action={
        <span onClick={handleClick}>
          {subText}
        </span>
      } />
  );
}

export default Alert;
