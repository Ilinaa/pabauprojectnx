
import Card  from './card';
import 'antd/dist/antd.css';
import { ReactComponent as Acne } from '../../../../../apps/online-booking/src/assets/acne.svg'

export default {
  component: Card,
  title: 'Card', 
  argTypes: {
    textAlign: {
      options: ['center','left ', 'right'],
      control: { type: 'select' }
    },
  }
};

const Template = (args) => <Card {...args} />;

export const Primary = Template.bind({})

Primary.args = {
  width:"10%",
  title:"Injectables",
  icon:<Acne/>,
  margin:"0px 10px",
  padding:"10px",
}