import styled from 'styled-components';
import { Card as CardItem} from 'antd';

const { Meta } = CardItem;

const StyledCard = styled(CardItem)`
  width:${props=>props.width};
  margin:${props=> props.margin};
  padding:${props=>props.padding};
  text-align:${props=>props.textAlign};
  .ant-card-body{
    padding:${props=> props.padding}
  }
`;

export function Card({
  width,
  title,
  handleClick,
  icon,
  classProperty,
  data,
  children,
  margin,
  padding,
  textAlign
}) {
  return (
    <StyledCard
      width={width}
      hoverable
      className={classProperty}
      cover={icon}
      className={classProperty}
      onClick={()=> handleClick(data)}
      margin={margin}
      padding={padding}
      textAlign={textAlign}
    >
      <Meta title={title} />
      {children}
    </StyledCard>
  )};
export default Card;
