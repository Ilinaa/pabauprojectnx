
import Button  from './button';
import { CheckCircleOutlined} from '@ant-design/icons';
import 'antd/dist/antd.css';

export default {
  component: Button,
  title: 'Button', 
  argTypes: {
    size: {
      options: ['defautl','large', 'small'],
      control: { type: 'select' }
    },
    type:{
      options: ['primary','default',"ghost","dashed","text","link"],
      control: { type: 'select' }
    },
    background: {
      control: 'color',
    },
    color:{
      control: 'color',
    },
    hoverBackground: {
      control: 'color',
    },
    hoverColor: {
      control: 'color',
    },
    shape: {
      options: ['round','circle','default'],
      control: { type: 'radio' }
    },
    disabled:{
      options: ['true','false'],
      control: { type: 'boolean'}
    },
  }
};

const Template = (args) => <Button {...args} />;

export const Primary = Template.bind({})

Primary.args = {
  icon: <CheckCircleOutlined />, 
  label:"Selected", 
}