import styled from 'styled-components';
import { Button as ButtonWrapper } from 'antd';


const StyledButton = styled(ButtonWrapper)`
  width:${props=>props.width};
  background:${props => props.background};
  color: ${props => props.color};
  border-color:${props=>props.borderColor ? props.borderColor : "none"};
  border-radius:${props=>props.shape ? props.shape : "none"};
  &:hover{
    background:${props => props.hoverBackground ? props.hoverBackground : props.background};
    color: ${props => props.hoverColor ? props.hoverColor : props.color};
    border-color:${props=>props.borderColorHover ? props.borderColorHover : "none"};
  };
`;


export function Button({
  size,
  icon,
  type,
  label,
  color,
  background,
  hoverColor,
  hoverBackground,
  hoverBorder,
  iconAfter,
  handleClick,
  classPropery,
  width,
  borderColor,
  borderColorHover,
  shape,
  disabled,
}) {

  return (
    <StyledButton
      type={type}
      width={width}
      size={size}
      icon={icon}
      color={color}
      background={background}
      hoverColor={hoverColor}
      hoverBackground={hoverBackground}
      hoverBorder={hoverBorder}
      onClick={() => handleClick()}
      classPropery={classPropery}
      borderColor={borderColor}
      borderColorHover={borderColorHover}
      shape={shape}
      disabled={disabled}
    >
      {label}
      {iconAfter}
    </StyledButton >
  );
}
export default Button;
