import styled from 'styled-components';


export const BoxStyle = styled.div`
    max_height:${props => props.height ? props.height : "605px"};
    overflow-y:${props => props.overflow ? props.height : "auto"};
    background: #fff;
    padding: 20px; 
    margin-right: 16px;
    margin-top:${props => props.marginTop && "10px"};
    .phone{
        color:#54b2d3;
        margin-left:5px;
    }
`

export function Style({
  height,
  overflow,
  background,
  padding,
  margin,
  marginTop,
  color
}) {
  return (
    <BoxStyle
      height={height}
      overflow={overflow}
      background={overflow}
    />
  )};
export default Style;