import { MedicineBoxOutlined, LaptopOutlined } from '@ant-design/icons';
let ContentData = {
    Header: {
        text: "Choose Service",
        span: "Step 1/8",
    },
    SubCategoryChoices: [{ name: "In Clinic",icon:MedicineBoxOutlined},{ name:"Virtual Consultation",icon:LaptopOutlined}],
    SubFilterData:{
        selectedMessage:"You may need a patch test",
        moreInfo:"more info"
    },
    MoreInfoModal:{
        heading:"Patch test",
        text:"To make sure your skin doesen’t react to the products used in your treatment, please book a patch test for at least 48 hours before your appointment.To make sure your skin doesen’t react to the products used in your treatment, please book a patch test for at least 48 hours before your appointment.",
        button:"I understand"
    },
    Selected:{
        box_button:"Selected",
        span:"services",
        button:"Next"
    },
    Contact:{
        text:"Not sure about consultation type? Please, call",
        phone:"045787498450"
    },
    Footer:{
        text:"Powered By Pabau"
    }
  

}

export default ContentData
