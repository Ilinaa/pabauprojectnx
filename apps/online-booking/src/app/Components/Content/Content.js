import { useState, useEffect } from 'react';
import { List } from 'antd';
import { ArrowRightOutlined } from '@ant-design/icons';
import { Row, Col } from 'antd';
import MainTopFilter from '../MainTopFilter/MainTopFilter';
import SubFilterData from '../SubFilterData/SubFilterData';
import SubFilter from '../SubFilter/SubFilter';
import styled from 'styled-components';
import ContentData from 'apps/online-booking/src/ContentData/ContentData';
import { BoxStyle } from '@pabau/ui';
import { Button } from '@pabau/ui'

const ContentWrapper = styled.div`
  background: #f7f7f9;
  padding: 50px 0px;
  .content_bottom{
    margin: 32px 0px 100px;
  }
  .fixed_top{
    width: 100%;
    position: fixed !important;
    bottom: 45px;
    .price_popUp{
      border-radius: 4px 4px 0px 0px;
      display: flex !important;
      align-items: center !important;
      background: #fff;
      margin-right: 16px !important;
      justify-content: space-between !important;
      padding: 16px 20px;
      box-shadow: 0 0 3px 0 rgba(0,0,0,.2784313725490196),0 0 0 1px rgba(0,0,0,.0784313725490196)!important;
      p, .button{
        text-align: right !important;
        margin: 0px;

      }
      p{
        color: #54b2d3;
        font-size: 20px;
        font-weight: 500;
    
      }

    }
  }
`
const PricePopUp = styled.div`
    border-radius: 4px 4px 0px 0px;
    display: flex !important;
    align-items: center !important;
    background: #fff;
    justify-content: space-between !important;
    padding: 16px 20px;
    box-shadow: 0 0 3px 0 rgba(0,0,0,.2784313725490196),0 0 0 1px rgba(0,0,0,.0784313725490196)!important;
    margin-right: 16px !important;
    .p , button{
      text-align: right !important;
      margin: 0px;
    }
    p{
      color: #54b2d3;
      font-size: 20px;
      font-weight: 500;
    }
`


const Content = () => {
  const [mainFilter, setmainFilter] = useState();
  const [filterdSubCategory, setFilteredSubCategory] = useState()
  const [active, setActive] = useState({ mainCategory: "All", category: "Botox", subCategory: "In Clinic" })
  const [status, setStatus] = useState("In Clinic")
  const [checkOnline, setCheckOnline] = useState(false);
  const [isSelectedList, setisSelectedList] = useState([]);
  const [showPrice, setshowPrice] = useState(0)


  const handleSelected = (data) => {
    let actualPrice = parseInt(data.price.split("-")[0]);
    let selectedItems = [...isSelectedList];

    // Checks if the name is in the list, if it is, then finds it and remove it, or add it to list

    if (selectedItems.includes(data.name)) {
      let index = selectedItems.indexOf(data.name)
      if (index !== -1) {
        selectedItems.splice(index, 1)
        setshowPrice(showPrice - actualPrice)
      }
    } else {
      selectedItems.push(data.name)
      setshowPrice(actualPrice + showPrice)
    }
    setisSelectedList(selectedItems)
  }

  useEffect(() => {
    setStatus("In Clinic")
  }, [filterdSubCategory])

  return (
    <ContentWrapper>
      <Row>
        <Col span={12} offset={6}>
          <MainTopFilter
            setmainFilter={setmainFilter}
            active={active}
            setActive={setActive}
          />
        </Col>
      </Row>
      <Row className="content_bottom">
        <Col span={6} offset={6} >
          <BoxStyle scroll>
            {mainFilter && mainFilter.map((filterItem, id) =>
              <SubFilter
                key={id}
                category={filterItem.category}
                setFilteredSubCategory={setFilteredSubCategory}
                active={active}
                setActive={setActive}
              />
            )}
          </BoxStyle>
        </Col>
        <Col span={6}>
          <BoxStyle>
            {(checkOnline ? [ContentData.SubCategoryChoices[0]] : ContentData.SubCategoryChoices).map((item, index) => (
              <Button
                key="index"
                label={item.name}
                width="50%"
                size="large"
                type="default"
                background="transparent"
                color="#737387"
                hoverBackground="transparent"
                hoverColor="#737387"
                borderColorHover="#d9d9d9"
                classPropery={`list_style ${checkOnline && "max_width"} ${status === item.name && "active"}`}
                handleClick={() => setStatus(item.name)}
              />
            ))
            }
            {filterdSubCategory && filterdSubCategory.map((subCategory, id) => (
              subCategory.subCategory.map((el, index) => (
                <SubFilterData
                  subCategory={el}
                  status={status}
                  key={index}
                  setCheckOnline={setCheckOnline}
                  handleSelected={(data) => handleSelected(data)}
                  isSelectedList={isSelectedList}
                />
              ))
            ))}
          </BoxStyle>
          <Row>
            <Col span={24}>
              <BoxStyle marginTop>
                <p>{ContentData.Contact.text} <span className="phone">{ContentData.Contact.phone}</span></p>
              </BoxStyle>
            </Col>
          </Row>
        </Col>
      </Row>
      {showPrice > 0 &&
        <Row className="fixed_top">
          <Col span={12} offset={6}>
            <Row className="price_popUp">
              <Col span={14}>
                <p> {isSelectedList.length} {ContentData.Selected.span} &#163; {showPrice}</p>
              </Col>
              <Col span={10} className="button">
                <Button
                  iconAfter={<ArrowRightOutlined />}
                  label={ContentData.Selected.button}
                  handleClick={() => console.log("Ok ,iam a button")}
                  background="#54b2d3"
                  color="#fff"
                  hoverBackground="transparent"
                  hoverColor="#7ec9e0"
                  hoverBorder="1px solid #7ec9e0"
                />
              </Col>
            </Row>
          </Col>
        </Row>
      }
    </ContentWrapper >
  );
}

export default Content;
