import { Col, Row } from 'antd';
import ContentData from 'apps/online-booking/src/ContentData/ContentData';
import styled from 'styled-components';



const FooterWrapper = styled(Row)`
    background: #fff;
    height:44px;
    width: 100%;
    position: fixed;
    bottom: 0px;
    .footer{
        display:flex;
        justify-content: center;
        align-items:center;
        a{
           color:#3d3d46;
           font-weight: 500;
        }
    }
`
const Footer = () => {
    return (
        <FooterWrapper>
            <Col span={24} className="footer">
                <a href="https://www.pabau.com" target="_blank" rel="noopener noreferrer">
                    {ContentData.Footer.text}
                </a>
            </Col>
        </FooterWrapper>

    );
}

export default Footer;