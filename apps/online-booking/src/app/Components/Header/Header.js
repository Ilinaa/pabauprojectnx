import { Typography } from 'antd';
import ContentData from 'apps/online-booking/src/ContentData/ContentData';
import styled from 'styled-components';


const { Title ,Text} = Typography;

const HeaderWrapper=styled.div`
    text-align: center;
    padding: 20px;
    background: #fff;
    h5{
        font-size:18px; 
    }
    span{
        font-size: 16px;
        line-height: 20px;
        color: #737387;
    }
`

const Header = () => {
    return (
        <HeaderWrapper>
         <Title level={5}>{ContentData.Header.text}</Title>
         <Text>{ContentData.Header.span}</Text>
        </HeaderWrapper>
    );
}
 
export default Header;