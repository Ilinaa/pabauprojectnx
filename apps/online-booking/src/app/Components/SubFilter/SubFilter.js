import  { useEffect } from 'react';
import { List } from 'antd';
import useFilter from '../Hooks/useFilter';


const SubFilter = ({ category, setFilteredSubCategory, active, setActive }) => {


    const [filtered, handleFilter] = useFilter(category)
    
    
    useEffect(() => {
        if(filtered.length === 1){
          setFilteredSubCategory(filtered)
        }
    }, [filtered])
    
    
    useEffect(() => {
        if (active.category === category[0].name) {
            setFilteredSubCategory([category[0]])
        }
    }, [category])
    
    
    return (
        <List
            size="medium"
            dataSource={category}
            renderItem={item =>
                <List.Item key={item.name}
                    className={`list_style ${active && active.category === item.name && "active"}`}
                    onClick={() => { handleFilter(item.name); setActive({ mainCategory: active.mainCategory, category: item.name, subCategory: "In Clinic" }) }}
                >
                    {item.name} <span>{item.rdmValue}</span>
                </List.Item>}
        />
    );
}

export default SubFilter;


