import { CheckCircleOutlined, ClockCircleOutlined, QuestionCircleOutlined, StarFilled } from '@ant-design/icons';
import { Modal } from 'antd';
import ContentData from 'apps/online-booking/src/ContentData/ContentData';
import usePopUp from '../Hooks/usePopUp';
import { Button } from '@pabau/ui'
import { Alert } from '@pabau/ui';
import { Card } from '@pabau/ui';
import styled from 'styled-components';

const BoxWrapper = styled.div`

    .box_middle,.box_bottom{
        text-align: left !important;
        padding: 7px 0px;
    }
    .box_top ,.box_bottom{
        display: flex;
        justify-content: space-between;
    }
    .box_top .box_span_name{
        font-size: 20px;
        line-height: 24px;
        color: #3d3d46;
        margin-right: 5px;
        text-transform: capitalize;
    }
    .box_top .box_span_right{
        font-weight: 500;
        font-size: 16px;
        line-height: 24px;
        color: #65cd98;
    }
    .box_span_reviews{
        color: #737387;
        font-size: 16px;
    }
    .box_middle span{
        color: #737387;
        font-size: 16px;
        line-height: 20px;
    }
    .box_selected{
        text-align: right;
    }
    .box_selected button{
        background-color: #54b2d3 !important;
        border-radius: 4px !important;
        color: #fff !important;
        font-size: 14px;
        font-weight: 700;
    }
    .box_bottom .review{
        margin-left:10px;
    }
`;


const Selected = ({ isSelectedList, button, name, handlePopUp }) => {
    return (
        isSelectedList.map((el, i) => (
            el === name ?
                button ?
                    <Button
                        size="default"
                        icon={<CheckCircleOutlined />}
                        label={ContentData.Selected.box_button}
                        handleClick={handlePopUp}
                        background="#54b2d3"
                        color="#fff"
                        hoverBackground="transparent"
                        hoverColor="#7ec9e0"
                        hoverBorder="1px solid #7ec9e0"
                        borderRadius="4px"
                    />
                    :
                    <Alert
                        key={i}
                        message={ContentData.SubFilterData.selectedMessage}
                        subText={ContentData.SubFilterData.moreInfo}
                        messageColor="rgba(0,0,0,.65)"
                        type="info"
                        backgroundCustom="#e2f7f5"
                        handleClick={handlePopUp}
                        colorText="rgba(0,0,0,.65)"
                        colorImg="rgb(32, 186, 177)"
                        subTextColor="rgba(0,0,0,.65)"
                    />
                : null
        ))
    )
}
const Box = ({ name, price, time, review, handleSelected, isSelectedList }) => {

    const [popUp, handlePopUp] = usePopUp();

    return (
        <BoxWrapper>
            <Card
                hoverable
                handleClick={handleSelected}
                data={{ name, price }}
                color="#3d3d46"
                margin="16px 0px"
            >
                <div className="box_top">
                    <div>
                        <span className="box_span_name">{name} </span>
                        <span><QuestionCircleOutlined /></span>
                    </div>
                    <span className="box_span_right">&#163; {price}</span>
                </div>
                <div className="box_middle">
                    <span>{time} min <span><ClockCircleOutlined /></span></span>
                </div>
                <div className="box_bottom">
                    <div className="box_span_reviews">
                        {([...Array(5)].map((x, i) => <StarFilled key={i} style={{ color: "#fadb14", fontSize: "20px" }} />))}
                        <span className="review">{review} review</span>
                    </div>
                    <Selected isSelectedList={isSelectedList} button handlePopUp={handlePopUp} name={name} />
                </div>
            </Card>

            <Selected isSelectedList={isSelectedList} handlePopUp={handlePopUp} name={name} />
            <Modal title={ContentData.MoreInfoModal.heading} visible={popUp} onCancel={handlePopUp}
                footer={[
                    <Button
                        size="large"
                        key="back"
                        handleClick={handlePopUp}
                        label={ContentData.MoreInfoModal.button}
                        background="#54b2d3"
                        color="#fff"
                        hoverBackground="#7ec9e0"
                        hoverColor="#fff"
                    />
                ]}
            >
                <p>{ContentData.MoreInfoModal.text}</p>
            </Modal>
        </BoxWrapper>
    );
}

export default Box;