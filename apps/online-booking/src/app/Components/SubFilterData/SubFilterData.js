import { useEffect } from 'react';
import Box from './Box';


const SubFilterData = ({ subCategory, status, setCheckOnline, handleSelected, isSelectedList }) => {

    useEffect(() => {
        if (subCategory.online === undefined) {
            setCheckOnline(true)
        } else {
            setCheckOnline(false)
        }
    }, [subCategory])

    return (
        <>
            {(status === "In Clinic" && !subCategory.online) ?
                <Box
                    name={subCategory.name}
                    price={subCategory.price}
                    time={subCategory.time}
                    review={subCategory.review}
                    handleSelected={handleSelected}
                    isSelectedList={isSelectedList} />
                : (status === "Virtual Consultation" && subCategory.online) ?
                    <Box
                        name={subCategory.name}
                        price={subCategory.price}
                        time={subCategory.time}
                        review={subCategory.review}
                        handleSelected={handleSelected}
                        isSelectedList={isSelectedList} />
                    :
                    null
            }
        </>
    );
}

export default SubFilterData;