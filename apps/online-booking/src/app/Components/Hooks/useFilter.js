import { defaultItems } from "apps/online-booking/src/data";
import { useState } from "react";


const useFilter = (data) => {
  const [filtered, setFiltered] = useState(data);


  const handleFilter = (name)=>{

    if(name === "All"){
      setFiltered(defaultItems) 
    }else{
      let filterData = data.filter((product)=> product.name===name)
      setFiltered(filterData)
    }
 }
 
  return [filtered,handleFilter];
};

export default useFilter;