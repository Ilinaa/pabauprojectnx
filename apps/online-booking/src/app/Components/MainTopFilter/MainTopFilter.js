import { Col, Row } from 'antd';
import { useEffect } from 'react';
import { AppstoreOutlined, CreditCardOutlined } from '@ant-design/icons';
import useFilter from '../Hooks/useFilter';
import { defaultItems } from 'apps/online-booking/src/data';
import { Card } from "@pabau/ui"
import { BoxStyle } from '@pabau/ui';


const MainTopFilter = ({ setmainFilter, active, setActive }) => {

   const [filtered, handleFilter] = useFilter(defaultItems)

   useEffect(() => {
      setmainFilter(filtered)
   }, [filtered])

   const handleClick = (data) => {
      handleFilter(data.title)
      setActive({ mainCategory: data.title, category: data.subCategory, subCategory: "In Clinic" })
  }

  return (
      <BoxStyle>
         <Row>
            <Col span={3} >
               <Card
                  width="100%"
                  title="All"
                  classProperty={`top_card ${active && active.mainCategory === "All" && "active"}`}
                  handleClick={handleClick}
                  setActive={setActive}
                  icon={<AppstoreOutlined style={{ fontSize: '40px', color: "rgb(255, 203, 100)" }} />} 
                  data={{title:"All", subCategory:"Botox"}}
                  margin="0px 10px"
                  padding="10px"
                  textAlign="center"
                  />
               </Col>
            {defaultItems.map((el, index) => (
               <Col span={3} key={index}>
                  <Card
                     title={el.name}
                     classProperty={`top_card ${active && active.mainCategory === el.name && "active"}`}
                     handleClick={handleClick}
                     setActive={setActive}
                     icon={<el.icon />}
                     data={{title: el.name, subCategory: el.category[0].name}}
                     margin="0px 10px"
                     padding="10px"
                     textAlign="center"
                  />
               </Col>
            ))
            }
            <Col span={3}>
               <Card
                  title="Voucher"
                  classProperty={`top_card ${active && active.mainCategory === "Voucher" && "active"}`}
                  handleClick={handleClick}
                  setActive={setActive}
                  icon={<CreditCardOutlined style={{ fontSize: '40px', color: "rgb(255, 203, 100)" }} />} 
                  data={{title:"All", subCategory:"Botox"}}
                  margin="0px 10px"
                  padding="10px"
                  textAlign="center"
                  />
                  
            </Col>
         </Row>
      </BoxStyle>
   );
}

export default MainTopFilter;