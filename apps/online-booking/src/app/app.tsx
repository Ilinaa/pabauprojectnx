import styles from './app.module.css';
import 'antd/dist/antd.css';
import { Layout } from 'antd';
import Header from './Components/Header/Header'
import Content from './Components/Content/Content'
import Footer from './Components/Footer/Footer'


export function App() {
  return (
    <Layout className={styles.app}>
      <Header />
      <Content/>
      <Footer/>
    </Layout>

  );
}

export default App;
